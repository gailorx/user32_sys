#![feature(libc)]
extern crate libc;
extern crate win32_types;
extern crate gdi32_sys;

mod user32;
pub use user32::*;
