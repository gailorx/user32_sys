#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use win32_types::{ULONG_PTR, LPCWSTR, WORD, HINSTANCE, HICON, HCURSOR};

pub fn MAKEINTRESOURCEW(resource: WORD) -> LPCWSTR {
    resource as ULONG_PTR as LPCWSTR
}

pub mod IDC {
    use win32_types::{LPCWSTR, WORD, ULONG_PTR};
    //NOTE: should use MAKEINTRESOURCEW but compile time functions do not exist
    //      like C++
    pub const ARROW: LPCWSTR = 32512 as WORD as ULONG_PTR as LPCWSTR;
}

pub mod IDI {
    use win32_types::{LPCWSTR, WORD, ULONG_PTR};

    pub const WINLOGO: LPCWSTR = 32517 as WORD as ULONG_PTR as LPCWSTR;
}


#[link(name = "user32")]
extern "stdcall" {
    pub fn LoadIconW(hInstance: HINSTANCE, lpIconName: LPCWSTR) -> HICON;
    pub fn LoadCursorW(hInstance: HINSTANCE, lpCursorName: LPCWSTR) -> HCURSOR;
}
