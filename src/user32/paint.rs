#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use win32_types::{HDC, HWND, RECT, LPRECT, HBRUSH, BYTE, BOOL};
use libc::c_int;

#[repr(C)]
pub struct PAINTSTRUCT {
    pub hdc: HDC,
    pub fErase: BOOL,
    pub rcPaint: RECT,
    pub fRestore: BOOL,
    pub fIncUpdate: BOOL,
    pub rgbReserved: [BYTE; 32],
}
impl Copy for PAINTSTRUCT {}

#[link(name = "user32")]
extern "stdcall" {
    pub fn ValidateRect(hWnd: HWND, lpRect: *const RECT) -> BOOL;
    pub fn GetUpdateRect(hWnd: HWND, lpRect: LPRECT, bErase: BOOL) -> BOOL;
    pub fn FillRect(hDC: HDC, lprc: *const RECT, hbr: HBRUSH) -> c_int;
    pub fn BeginPaint(hwnd: HWND, lpPaint: *mut PAINTSTRUCT) -> HDC;
    pub fn EndPaint(hWnd: HWND, lpPaint: *const PAINTSTRUCT) -> BOOL;
}
