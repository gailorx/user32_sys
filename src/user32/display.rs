#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use win32_types::{LONG, LPVOID, BOOL, LPCWSTR, DWORD, RECT, HWND, HMONITOR};
use gdi32_sys::{DISPLAY_DEVICEW, DEVMODE};

#[repr(C)]
pub struct MONITORINFO {
    pub cbSize: DWORD,
    pub rcMonitor: RECT,
    pub rcWork: RECT,
    pub dwFlags: DWORD,
}
impl Copy for MONITORINFO {}
pub type LPMONITORINFO = *mut MONITORINFO;

pub mod MONITOR {
    use win32_types::DWORD;

    pub const DEFAULTTONULL: DWORD = 0;//Returns NULL.
    pub const DEFAULTTOPRIMARY: DWORD = 1;//Returns a handle to the primary display monitor.
    pub const DEFAULTTONEAREST: DWORD = 2;// Returns a handle to the display monitor that is nearest to the window.
}

pub mod DISPLAY_DEVICE {
    use win32_types::DWORD;

    pub const ACTIVE: DWORD = 0x00000001;
    pub const MULTI_DRIVER: DWORD = 0x00000002;
    pub const PRIMARY_DEVICE: DWORD = 0x00000004;
    pub const MIRRORING_DRIVER: DWORD = 0x00000008;
    pub const VGA_COMPATIBLE: DWORD = 0x00000010;
}

pub mod DM {
    use win32_types::DWORD;

    pub const ORIENTATION: DWORD = 0x00000001;
    pub const PAPERSIZE: DWORD = 0x00000002;
    pub const PAPERLENGTH: DWORD = 0x00000004;
    pub const PAPERWIDTH: DWORD = 0x00000008;
    pub const SCALE: DWORD = 0x00000010;
    pub const POSITION: DWORD = 0x00000020;
    pub const NUP: DWORD = 0x00000040;
    pub const DISPLAYORIENTATION: DWORD = 0x00000080;
    pub const COPIES: DWORD = 0x00000100;
    pub const DEFAULTSOURCE: DWORD = 0x00000200;
    pub const PRINTQUALITY: DWORD = 0x00000400;
    pub const COLOR: DWORD = 0x00000800;
    pub const DUPLEX: DWORD = 0x00001000;
    pub const YRESOLUTION: DWORD = 0x00002000;
    pub const TTOPTION: DWORD = 0x00004000;
    pub const COLLATE: DWORD = 0x00008000;
    pub const FORMNAME: DWORD = 0x00010000;
    pub const LOGPIXELS: DWORD = 0x00020000;
    pub const BITSPERPEL: DWORD = 0x00040000;
    pub const PELSWIDTH: DWORD = 0x00080000;
    pub const PELSHEIGHT: DWORD = 0x00100000;
    pub const DISPLAYFLAGS: DWORD = 0x00200000;
    pub const DISPLAYFREQUENCY: DWORD = 0x00400000;
    pub const ICMMETHOD: DWORD = 0x00800000;
    pub const ICMINTENT: DWORD = 0x01000000;
    pub const MEDIATYPE: DWORD = 0x02000000;
    pub const DITHERTYPE: DWORD = 0x04000000;
    pub const PANNINGWIDTH: DWORD = 0x08000000;
    pub const PANNINGHEIGHT: DWORD = 0x10000000;
    pub const DISPLAYFIXEDOUTPUT: DWORD = 0x20000000;
}

pub mod ENUM {
    use win32_types::DWORD;

    pub const CURRENT_SETTINGS: DWORD = -1;
    pub const REGISTRY_SETTINGS: DWORD = -2;
}


#[link(name = "user32")]
extern "stdcall" {
    pub fn MonitorFromWindow(hwnd: HWND, dwFlags: DWORD) -> HMONITOR;
    pub fn GetMonitorInfoW(hMonitor: HMONITOR, lpmi: LPMONITORINFO) -> BOOL;

    pub fn EnumDisplayDevicesW(lpDevice: LPCWSTR, iDevNum: DWORD, lpDisplayDevice: *mut DISPLAY_DEVICEW, dwFlags: DWORD) -> BOOL;
    pub fn EnumDisplaySettingsExW(lpszDeviceName: LPCWSTR, iModeNum: DWORD, lpDevMode: *mut DEVMODE, dwFlags: DWORD) -> BOOL;
    pub fn ChangeDisplaySettingsW(lpDevMode: *mut DEVMODE, dwFlags: DWORD) -> LONG;
    pub fn ChangeDisplaySettingsExW(lpszDeviceName: LPCWSTR, lpDevMode: *mut DEVMODE, hwnd: HWND, dwFlags: DWORD, lParam: LPVOID) -> LONG;
}
