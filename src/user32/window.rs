#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use win32_types::{HWND, HDC, LONG_PTR, BOOL, LPCWSTR, HMENU, HINSTANCE, RECT, DWORD, UINT, LPVOID, LPRECT, POINT};
use libc::c_int;

#[repr(C)]
pub struct WINDOWPLACEMENT {
    pub length: UINT,
    pub flags: UINT,
    pub showCmd: UINT,
    pub ptMinPosition: POINT,
    pub ptMaxPosition: POINT,
    pub rcNormalPosition: RECT,
}
impl Copy for WINDOWPLACEMENT {}
pub type PWINDOWPLACEMENT = *mut WINDOWPLACEMENT;
pub type LPWINDOWPLACEMENT = PWINDOWPLACEMENT;


pub mod Z_ORDER {
    use win32_types::HWND;

    pub const HWND_TOP: HWND = 0 as HWND;
    pub const HWND_BOTTOM: HWND = 1 as HWND;
    pub const HWND_TOPMOST: HWND = -1 as HWND;
    pub const HWND_NOTOPMOST: HWND = -2 as HWND;
}

pub mod SW {
    use libc::c_int;

    pub const FORCEMINIMIZE: c_int = 11;
    pub const HIDE: c_int = 0;
    pub const MAXIMIZE: c_int = 3;
    pub const MINIMIZE: c_int = 6;
    pub const RESTORE: c_int = 9;
    pub const SHOW: c_int = 5;
    pub const SHOWDEFAULT: c_int = 10;
    pub const SHOWMAXIMIZED: c_int = 3;
    pub const SHOWMINIMIZED: c_int = 2;
    pub const SHOWMINNOACTIVE: c_int = 7;
    pub const SHOWNA: c_int = 8;
    pub const SHOWNOACTIVATE: c_int = 4;
    pub const SHOWNORMAL: c_int = 1;
}

pub mod SWP {
    use win32_types::UINT;

    pub const ASYNCWINDOWPOS: UINT = 0x4000;
    pub const DEFERERASE: UINT = 0x2000;
    pub const DRAWFRAME: UINT = 0x0020;
    pub const FRAMECHANGED: UINT = 0x0020;
    pub const HIDEWINDOW: UINT = 0x0080;
    pub const NOACTIVATE: UINT = 0x0010;
    pub const NOCOPYBITS: UINT = 0x0100;
    pub const NOMOVE: UINT = 0x0002;
    pub const NOOWNERZORDER: UINT = 0x0200;
    pub const NOREDRAW: UINT = 0x0008;
    pub const NOREPOSITION: UINT = 0x0200;
    pub const NOSENDCHANGING: UINT = 0x0400;
    pub const NOSIZE: UINT = 0x0001;
    pub const NOZORDER: UINT = 0x0004;
    pub const SHOWWINDOW: UINT = 0x0040;
}

pub mod GWL {
    use libc::c_int;

    pub const EXSTYLE: c_int = -20;
    pub const STYLE: c_int = -16;
}

pub mod GWLP {
    use libc::c_int;

	pub const USERDATA: c_int = -21;
    pub const ID: c_int = -12;
    pub const WNDPROC: c_int = -4;
    pub const HINSTANCE: c_int = -6;
    pub const HWNDPARENT: c_int = -8;
}

// http://msdn.microsoft.com/en-us/library/windows/desktop/ms632600(v=vs.85).aspx
pub mod WS {
    use win32_types::DWORD;

    pub const BORDER: DWORD = 0x00800000;
    pub const CAPTION: DWORD = 0x00C00000;
    pub const CHILD: DWORD = 0x40000000;
    pub const CHILDWINDOW: DWORD = 0x40000000;
    pub const CLIPCHILDREN: DWORD = 0x02000000;
    pub const CLIPSIBLINGS: DWORD = 0x04000000;
    pub const DISABLED: DWORD = 0x08000000;
    pub const DLGFRAME: DWORD = 0x00400000;
    pub const GROUP: DWORD = 0x00020000;
    pub const HSCROLL: DWORD = 0x00100000;
    pub const ICONIC: DWORD = 0x20000000;
    pub const MAXIMIZE: DWORD = 0x01000000;
    pub const MAXIMIZEBOX: DWORD = 0x00010000;
    pub const MINIMIZE: DWORD = 0x20000000;
    pub const MINIMIZEBOX: DWORD = 0x00020000;
    pub const OVERLAPPED: DWORD = 0x00000000;
    pub const OVERLAPPEDWINDOW: DWORD = (OVERLAPPED | CAPTION | SYSMENU | THICKFRAME | MINIMIZEBOX | MAXIMIZEBOX);
    pub const POPUP: DWORD = 0x80000000;
    pub const POPUPWINDOW: DWORD = (POPUP | BORDER | SYSMENU);
    pub const SIZEBOX: DWORD = 0x00040000;
    pub const SYSMENU: DWORD = 0x00080000;
    pub const TABSTOP: DWORD = 0x00010000;
    pub const THICKFRAME: DWORD = 0x00040000;
    pub const TILED: DWORD = 0x00000000;
    pub const TILEDWINDOW: DWORD = (OVERLAPPED | CAPTION | SYSMENU | THICKFRAME | MINIMIZEBOX | MAXIMIZEBOX);
    pub const VISIBLE: DWORD = 0x10000000;
    pub const VSCROLL: DWORD = 0x00200000;
}

// http://msdn.microsoft.com/en-us/library/windows/desktop/ff700543(v=vs.85).aspx
pub mod WS_EX {
    use win32_types::DWORD;

    pub const ACCEPTFILES: DWORD = 0x00000010;
    pub const APPWINDOW: DWORD = 0x00040000;
    pub const CLIENTEDGE: DWORD = 0x00000200;
    pub const COMPOSITED: DWORD = 0x02000000;
    pub const CONTEXTHELP: DWORD = 0x00000400;
    pub const CONTROLPARENT: DWORD = 0x00010000;
    pub const DLGMODALFRAME: DWORD = 0x00000001;
    pub const LAYERED: DWORD = 0x00080000;
    pub const LAYOUTRTL: DWORD = 0x00400000;
    pub const LEFT: DWORD = 0x00000000;
    pub const LEFTSCROLLBAR: DWORD = 0x00004000;
    pub const LTRREADING: DWORD = 0x00000000;
    pub const MDICHILD: DWORD = 0x00000040;
    pub const NOACTIVATE: DWORD = 0x08000000;
    pub const NOINHERITLAYOUT: DWORD = 0x00100000;
    pub const NOPARENTNOTIFY: DWORD = 0x00000004;
    pub const NOREDIRECTIONBITMAP: DWORD = 0x00200000;
    pub const OVERLAPPEDWINDOW: DWORD = (WINDOWEDGE | CLIENTEDGE);
    pub const PALETTEWINDOW: DWORD = (WINDOWEDGE | TOOLWINDOW | TOPMOST);
    pub const RIGHT: DWORD = 0x00001000;
    pub const RIGHTSCROLLBAR: DWORD = 0x00000000;
    pub const RTLREADING: DWORD = 0x00002000;
    pub const STATICEDGE: DWORD = 0x00020000;
    pub const TOOLWINDOW: DWORD = 0x00000080;
    pub const TOPMOST: DWORD = 0x00000008;
    pub const TRANSPARENT: DWORD = 0x00000020;
    pub const WINDOWEDGE: DWORD = 0x00000100;
}


#[link(name = "user32")]
extern "stdcall" {
    pub fn CreateWindowExW(extrastyle: DWORD, classname: LPCWSTR,
            windowname: LPCWSTR, style: DWORD,
            x: c_int, y: c_int, width: c_int, height: c_int,
            parent: HWND, menu: HMENU, instance: HINSTANCE, param: LPVOID
            ) -> HWND;
    pub fn DestroyWindow(window: HWND) -> BOOL;

    pub fn ShowWindow(window: HWND, nCmdShow: c_int) -> BOOL;
    pub fn ShowWindowAsync(window: HWND, nCmdShow: c_int) -> BOOL;
    pub fn UpdateWindow(window: HWND) -> BOOL;

    #[cfg(target_arch = "x86")]
    #[link_name = "GetClassLongW"]
    pub fn GetClassLongW(window: HWND, nIndex: c_int) -> DWORD;
    #[cfg(target_arch = "x86")]
    #[link_name = "SetClassLongW"]
    pub fn SetClassLongW(window: HWND, nIndex: c_int, dwNewLong: LONG) -> DWORD;
    #[cfg(target_arch = "x86_64")]
    #[link_name = "GetWindowLongPtrW"]
    pub fn GetWindowLongPtrW(hWnd: HWND, nIndex: c_int) -> LONG_PTR;
    #[cfg(target_arch = "x86_64")]
    #[link_name = "SetWindowLongPtrW"]
    pub fn SetWindowLongPtrW(hWnd: HWND, nIndex: c_int, dwNewLong: LONG_PTR) -> LONG_PTR;

    pub fn GetClientRect(window: HWND, rect: LPRECT) -> BOOL;

    pub fn SetWindowPos(window: HWND, hwndInsertAfter: HWND, x: c_int, y: c_int,
                                    cx: c_int, cy: c_int, flags: UINT) -> BOOL;

    pub fn SetFocus(window: HWND) -> HWND;

    pub fn GetDC(window: HWND) -> HDC;
    pub fn ReleaseDC(window: HWND, dc: HDC) -> c_int;

    pub fn SetWindowTextW(hWnd: HWND, lpString: LPCWSTR) -> BOOL;

    pub fn SetForegroundWindow(hWnd: HWND) -> BOOL;

    pub fn GetWindowRect(hWnd: HWND, lpRect: *mut RECT) -> BOOL;
    pub fn AdjustWindowRectEx(lpRect: *mut RECT, dwStyle: DWORD, bMenu: BOOL, dwExStyle: DWORD) -> BOOL;

    pub fn MessageBoxW(hWnd: HWND, lpText: LPCWSTR, lpCaption: LPCWSTR, uType: UINT) -> c_int;

    pub fn GetWindowPlacement(hWnd: HWND, lpwndpl: *mut WINDOWPLACEMENT) -> BOOL;
    pub fn SetWindowPlacement(hWnd: HWND, lpwndpl: *const WINDOWPLACEMENT) -> BOOL;
}
