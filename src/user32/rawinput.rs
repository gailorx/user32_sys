#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use win32_types::{DWORD, INT, UINT, PUINT, LONG, ULONG, USHORT, BYTE, WPARAM, HWND, HANDLE, BOOL, LRESULT, LPVOID};

pub mod RI_KEY {
    use win32_types::USHORT;

    pub const BREAK: USHORT = 1;
    pub const E0: USHORT = 2;
    pub const E1: USHORT = 4;
    pub const MAKE: USHORT = 0;
}

pub mod RI_MOUSE {
    use win32_types::USHORT;

    pub const LEFT_BUTTON_DOWN: USHORT = BUTTON_1_DOWN;
    pub const LEFT_BUTTON_UP: USHORT = BUTTON_1_UP;
    pub const MIDDLE_BUTTON_DOWN: USHORT = BUTTON_3_DOWN;
    pub const MIDDLE_BUTTON_UP: USHORT = BUTTON_3_UP;
    pub const RIGHT_BUTTON_DOWN: USHORT = BUTTON_2_DOWN;
    pub const RIGHT_BUTTON_UP: USHORT = BUTTON_2_UP;
    pub const BUTTON_1_DOWN: USHORT = 0x0001;
    pub const BUTTON_1_UP: USHORT = 0x0002;
    pub const BUTTON_2_DOWN: USHORT = 0x0004;
    pub const BUTTON_2_UP: USHORT = 0x0008;
    pub const BUTTON_3_DOWN: USHORT = 0x0010;
    pub const BUTTON_3_UP: USHORT = 0x0020;
    pub const BUTTON_4_DOWN: USHORT = 0x0040;
    pub const BUTTON_4_UP: USHORT = 0x0080;
    pub const BUTTON_5_DOWN: USHORT = 0x100;
    pub const BUTTON_5_UP: USHORT = 0x0200;
    pub const WHEEL: USHORT = 0x0400;
}

pub mod MOUSE {
    use win32_types::USHORT;

    pub const ATTRIBUTES_CHANGED: USHORT = 0x04;
    pub const MOVE_RELATIVE: USHORT = 0;
    pub const MOVE_ABSOLUTE: USHORT = 1;
    pub const VIRTUAL_DESKTOP: USHORT = 0x02;
}

pub mod RIDEV {
    use win32_types::UINT;

    pub const PAGEONLY: UINT = 0x00000020;
    pub const NOLEGACY: UINT = 0x00000030;
    pub const EXCLUDE: UINT = 0x00000010;
    pub const APPKEYS: UINT = 0x00000400;
    pub const CAPTUREMOUSE: UINT = 0x00000200;
    pub const DEVNOTIFY: UINT = 0x00002000;
    pub const EXINPUTSINK: UINT = 0x00001000;
    pub const INPUTSINK: UINT = 0x00000100;
    pub const NOHOTKEYS: UINT = 0x00000200;
    pub const REMOVE: UINT = 0x00000001;
}

pub mod RID {
    use win32_types::UINT;

    pub const HEADER: UINT = 0x10000005;
    pub const INPUT: UINT = 0x10000003;
}

pub mod RIDI {
    use win32_types::UINT;

    pub const DEVICENAME: UINT = 0x20000007;
    pub const DEVICEINFO: UINT = 0x2000000b;
    pub const PREPARSEDDATA: UINT = 0x20000005;
}

pub mod RIM {
    use win32_types::UINT;

    pub const INPUT: UINT = 0;
    pub const INPUTSINK: UINT = 1;
    
    pub const TYPEMOUSE: UINT = 0;
    pub const TYPEKEYBOARD: UINT = 1;
    pub const TYPEHID: UINT = 2;
}

#[repr(C)]
pub struct RAWINPUT {
    pub header: RAWINPUTHEADER,
    data: [u8; 24], // is 22 but padding makes it 24 (done by testing in c). Pretty dodgy.. think of alternative and at least write tests
    // And pray rust adds support for c unions somehow... please
        /*union {
            RAWMOUSE    mouse;
            RAWKEYBOARD keyboard;
            RAWHID      hid;
    } data; */
}
impl RAWINPUT {
    pub fn mouse(&self) -> &RAWMOUSE {
        unsafe { &(*(self.data.as_ptr() as *const RAWMOUSE)) }
    }
    pub fn keyboard(&self) -> &RAWKEYBOARD {
        unsafe { &(*(self.data.as_ptr() as *const RAWKEYBOARD)) }
    }
    pub fn hid(&self) -> &RAWHID {
        unsafe { &(*(self.data.as_ptr() as *const RAWHID)) }
    }
}
impl Copy for RAWINPUT {}
pub type PRAWINPUT = *mut RAWINPUT;
pub type HRAWINPUT = *mut RAWINPUT;
pub type LPRAWINPUT = *mut RAWINPUT;

#[repr(C)]
pub struct RAWINPUTHEADER {
    pub dwType: DWORD,
    pub dwSize: DWORD,
    pub hDevice: HANDLE,
    pub wParam: WPARAM,
}
impl Copy for RAWINPUTHEADER {}
pub type PRAWINPUTHEADER = *mut RAWINPUTHEADER;

#[repr(C)]
pub struct RAWKEYBOARD {
    pub MakeCode: USHORT,
    pub Flags: USHORT,
    Reserved: USHORT,
    pub VKey: USHORT,
    pub Message: UINT,
    pub ExtraInformation: ULONG,
}
impl Copy for RAWKEYBOARD {}
pub type PRAWKEYBOARD = *mut RAWKEYBOARD;
pub type LPRAWKEYBOARD = *mut RAWKEYBOARD;

#[repr(C)]
pub struct RAWHID {
    pub dwSizeHid: DWORD,
    pub dwCount: DWORD,
    pub bRawData: [BYTE; 1],
}
impl Copy for RAWHID {}
pub type PRAWHID = *mut RAWHID;
pub type LPRAWHID = *mut RAWHID;

#[repr(C)]
pub struct RAWMOUSE {
    pub usFlags: USHORT, // padded by 2 after this member
    /*union {
        uLONG  ulButtons;
        struct {
            uSHORT usButtonFlags;
            uSHORT usButtonData;
        };
    };*/
    data: [u8; 4],
    pub ulRawButtons: ULONG,
    pub lLastX: LONG,
    pub lLastY: LONG,
    pub ulExtraInformation: LONG,
}
impl Copy for RAWMOUSE {}
pub type PRAWMOUSE = *mut RAWMOUSE;
pub type LPRAWMOUSE = *mut RAWMOUSE;

impl RAWMOUSE {
    pub fn ulButtons(&self) -> ULONG {
        unsafe { *(self.data.as_ptr() as *const ULONG) }
    }
    pub fn usButtonFlags(&self) -> USHORT {
        unsafe { super::events::HIWORD(*(self.data.as_ptr() as *const DWORD)) }
    }
    pub fn usButtonData(&self) -> USHORT {
        unsafe { super::events::LOWORD(*(self.data.as_ptr() as *const DWORD)) }
    }
}

#[repr(C)]
pub struct RAWINPUTDEVICE {
    pub usUsagePage: USHORT,
    pub usUsage: USHORT,
    pub dwFlags: DWORD,
    pub hwndTarget: HWND,
}
impl Copy for RAWINPUTDEVICE {}
pub type PRAWINPUTDEVICE = *mut RAWINPUTDEVICE;
pub type PCRAWINPUTDEVICE = *const RAWINPUTDEVICE;
pub type LPRAWINPUTDEVICE = *mut RAWINPUTDEVICE;

#[repr(C)]
pub struct RAWINPUTDEVICELIST {
    pub hDevice: HANDLE,
    pub dwType: DWORD,
}
impl Copy for RAWINPUTDEVICELIST {}
pub type PRAWINPUTDEVICELIST = *mut RAWINPUTDEVICELIST;

   #[repr(C)]
pub struct RID_DEVICE_INFO {
    pub cbSize: DWORD,
    pub dwType: DWORD,
    data: [u8; 24],
    /* Actual C Fields
    pub cbSize: DWORD,
    pub dwType: DWORD,
    union {
        RID_DEVICE_INFO_MOUSE    mouse;
        RID_DEVICE_INFO_KEYBOARD keyboard;
        RID_DEVICE_INFO_HID      hid;
    };*/
}
impl Copy for RID_DEVICE_INFO {}
pub type PRID_DEVICE_INFO = *mut RID_DEVICE_INFO;
pub type LPRID_DEVICE_INFO = *mut RID_DEVICE_INFO;

impl RID_DEVICE_INFO {
    pub fn mouse(&self) -> &RID_DEVICE_INFO_MOUSE {
        unsafe { &(*(self.data.as_ptr() as *const RID_DEVICE_INFO_MOUSE)) }
    }

    pub fn keyboard(&self) -> &RID_DEVICE_INFO_KEYBOARD  {
        unsafe { &(*(self.data.as_ptr() as *const RID_DEVICE_INFO_KEYBOARD)) }
    }

    pub fn hid(&self) -> &RID_DEVICE_INFO_HID {
        unsafe { &(*(self.data.as_ptr() as *const RID_DEVICE_INFO_HID)) }
    }
}

#[repr(C)]
pub struct RID_DEVICE_INFO_HID {
    pub dwVendorId: DWORD,
    pub dwProductId: DWORD,
    pub dwVersionNumber: DWORD,
    pub usUsagePage: USHORT,
    pub usUsage: USHORT,
}
impl Copy for RID_DEVICE_INFO_HID {}
pub type PRID_DEVICE_INFO_HID = *mut RID_DEVICE_INFO_HID;

#[repr(C)]
pub struct RID_DEVICE_INFO_KEYBOARD {
    pub dwType: DWORD,
    pub dwSubType: DWORD,
    pub dwKeyboardMode: DWORD,
    pub dwNumberOfFunctionKeys: DWORD,
    pub dwNumberOfIndicators: DWORD,
    pub dwNumberOfKeysTotal: DWORD,
}
impl Copy for RID_DEVICE_INFO_KEYBOARD {}
pub type PRID_DEVICE_INFO_KEYBOARD = *mut RID_DEVICE_INFO_KEYBOARD;

#[repr(C)]
pub struct RID_DEVICE_INFO_MOUSE {
    pub dwId: DWORD,
    pub dwNumberOfButtons: DWORD,
    pub dwSampleRate: DWORD,
    pub fHasHorizontalWheel: BOOL,
}
impl Copy for RID_DEVICE_INFO_MOUSE {}
pub type PRID_DEVICE_INFO_MOUSE = *mut RID_DEVICE_INFO_MOUSE;

#[link(name = "user32")]
extern "stdcall" {
    pub fn DefRawInputProc(paRawInput: *mut PRAWINPUT, nInput: INT, cbSizeHeader: UINT) -> LRESULT;
    pub fn GetRawInputBuffer(pData: PRAWINPUT, pcbSize: PUINT, cbSizeHeader: UINT) -> UINT;
    pub fn GetRawInputData(hRawInput: HRAWINPUT, uiCommand: UINT, pData: LPVOID, pcbSize: PUINT, cbSizeHeader: UINT) -> UINT;
    pub fn GetRawInputDeviceInfoW(hDevice: HANDLE, uiCommand: UINT, pData: LPVOID, pcbSize: PUINT) -> UINT;
    pub fn GetRawInputDeviceList(pRawInputDeviceList: PRAWINPUTDEVICELIST, puiNumDevices: PUINT, cbSize: UINT) -> UINT;
    pub fn GetRegisteredRawInputDevices(pRawInputDevices: PRAWINPUTDEVICE, puiNumDevices: PUINT,  cbSize: UINT) -> UINT;
    pub fn RegisterRawInputDevices(pRawInputDevices: PCRAWINPUTDEVICE, uiNumDevices: UINT, cbSize: UINT) -> BOOL;
}