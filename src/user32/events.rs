#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use win32_types::{HWND, LONG, LONG_PTR, BOOL, WPARAM, LPARAM, LPCWSTR, HMENU, HINSTANCE, DWORD, WORD, UINT, LPVOID, LRESULT, POINT};
use libc::c_int;

pub fn LOWORD(l: DWORD) -> WORD {
    (l & 0xFFFF) as WORD
}

pub fn HIWORD(l: DWORD) -> WORD {
    (l >> 16) as WORD
}

pub fn GET_X_LPARAM(lp: LONG_PTR) -> c_int {
    LOWORD(lp as DWORD) as c_int
}

pub fn GET_Y_LPARAM(lp: LONG_PTR) -> c_int {
    HIWORD(lp as DWORD) as c_int
}

pub type WNDPROC = Option<unsafe extern "stdcall" fn(HWND, UINT, WPARAM, LPARAM) -> LRESULT>;

#[repr(C)]
pub struct MSG {
    pub window: HWND,
    pub message: UINT,
    pub wParam: WPARAM,
    pub lParam: LPARAM,
    pub time: DWORD,
    pub pt: POINT,
}
impl Copy for MSG {}

#[repr(C)]
pub struct CREATESTRUCT {
    pub lpCreateParams: LPVOID,
    pub hInstance: HINSTANCE,
    pub hMenu: HMENU,
    pub hwndParent: HWND,
    pub cy: c_int,
    pub cx: c_int,
    pub y: c_int,
    pub x: c_int,
    pub style: LONG,
    pub lpszName: LPCWSTR,
    pub lpszClass: LPCWSTR,
    pub dwExStyle: DWORD,
}
impl Copy for CREATESTRUCT {}
pub type LPCREATESTRUCT = *mut CREATESTRUCT;

pub mod PM {
    use win32_types::UINT;

    pub const NOREMOVE: UINT = 0x0000;
    pub const REMOVE: UINT = 0x0001;
    pub const NOYIELD: UINT = 0x0002;
}

pub mod VK {
    use win32_types::WPARAM;

    pub const LBUTTON: WPARAM = 0x01;
    pub const RBUTTON: WPARAM = 0x02;
    pub const CANCEL: WPARAM = 0x03;
    pub const MBUTTON: WPARAM = 0x04;
    pub const XBUTTON1: WPARAM = 0x05;
    pub const XBUTTON2: WPARAM = 0x06;
    pub const BACK: WPARAM = 0x08;
    pub const TAB: WPARAM = 0x09;
    pub const CLEAR: WPARAM = 0x0C;
    pub const RETURN: WPARAM = 0x0D;
    pub const SHIFT: WPARAM = 0x10;
    pub const CONTROL: WPARAM = 0x11;
    pub const MENU: WPARAM = 0x12;
    pub const PAUSE: WPARAM = 0x13;
    pub const CAPITAL: WPARAM = 0x14;
    pub const KANA: WPARAM = 0x15;
    pub const HANGUEL: WPARAM = 0x15;
    pub const HANGUL: WPARAM = 0x15;
    pub const JUNJA: WPARAM = 0x17;
    pub const FINAL: WPARAM = 0x18;
    pub const HANJA: WPARAM = 0x19;
    pub const KANJI: WPARAM = 0x19;
    pub const ESCAPE: WPARAM = 0x1B;
    pub const CONVERT: WPARAM = 0x1C;
    pub const NONCONVERT: WPARAM = 0x1D;
    pub const ACCEPT: WPARAM = 0x1E;
    pub const MODECHANGE: WPARAM = 0x1F;
    pub const SPACE: WPARAM = 0x20;
    pub const PRIOR: WPARAM = 0x21;
    pub const NEXT: WPARAM = 0x22;
    pub const END: WPARAM = 0x23;
    pub const HOME: WPARAM = 0x24;
    pub const LEFT: WPARAM = 0x25;
    pub const UP: WPARAM = 0x26;
    pub const RIGHT: WPARAM = 0x27;
    pub const DOWN: WPARAM = 0x28;
    pub const SELECT: WPARAM = 0x29;
    pub const PRINT: WPARAM = 0x2A;
    pub const EXECUTE: WPARAM = 0x2B;
    pub const SNAPSHOT: WPARAM = 0x2C;
    pub const INSERT: WPARAM = 0x2D;
    pub const DELETE: WPARAM = 0x2E;
    pub const HELP: WPARAM = 0x2F;
    pub const LWIN: WPARAM = 0x5B;
    pub const RWIN: WPARAM = 0x5C;
    pub const APPS: WPARAM = 0x5D;
    pub const SLEEP: WPARAM = 0x5F;
    pub const NUMPAD0: WPARAM = 0x60;
    pub const NUMPAD1: WPARAM = 0x61;
    pub const NUMPAD2: WPARAM = 0x62;
    pub const NUMPAD3: WPARAM = 0x63;
    pub const NUMPAD4: WPARAM = 0x64;
    pub const NUMPAD5: WPARAM = 0x65;
    pub const NUMPAD6: WPARAM = 0x66;
    pub const NUMPAD7: WPARAM = 0x67;
    pub const NUMPAD8: WPARAM = 0x68;
    pub const NUMPAD9: WPARAM = 0x69;
    pub const MULTIPLY: WPARAM = 0x6A;
    pub const ADD: WPARAM = 0x6B;
    pub const SEPARATOR: WPARAM = 0x6C;
    pub const SUBTRACT: WPARAM = 0x6D;
    pub const DECIMAL: WPARAM = 0x6E;
    pub const DIVIDE: WPARAM = 0x6F;
    pub const F1: WPARAM = 0x70;
    pub const F2: WPARAM = 0x71;
    pub const F3: WPARAM = 0x72;
    pub const F4: WPARAM = 0x73;
    pub const F5: WPARAM = 0x74;
    pub const F6: WPARAM = 0x75;
    pub const F7: WPARAM = 0x76;
    pub const F8: WPARAM = 0x77;
    pub const F9: WPARAM = 0x78;
    pub const F10: WPARAM = 0x79;
    pub const F11: WPARAM = 0x7A;
    pub const F12: WPARAM = 0x7B;
    pub const F13: WPARAM = 0x7C;
    pub const F14: WPARAM = 0x7D;
    pub const F15: WPARAM = 0x7E;
    pub const F16: WPARAM = 0x7F;
    pub const F17: WPARAM = 0x80;
    pub const F18: WPARAM = 0x81;
    pub const F19: WPARAM = 0x82;
    pub const F20: WPARAM = 0x83;
    pub const F21: WPARAM = 0x84;
    pub const F22: WPARAM = 0x85;
    pub const F23: WPARAM = 0x86;
    pub const F24: WPARAM = 0x87;
    pub const NUMLOCK: WPARAM = 0x90;
    pub const SCROLL: WPARAM = 0x91;
    pub const LSHIFT: WPARAM = 0xA0;
    pub const RSHIFT: WPARAM = 0xA1;
    pub const LCONTROL: WPARAM = 0xA2;
    pub const RCONTROL: WPARAM = 0xA3;
    pub const LMENU: WPARAM = 0xA4;
    pub const RMENU: WPARAM = 0xA5;
    pub const BROWSER_BACK: WPARAM = 0xA6;
    pub const BROWSER_FORWARD: WPARAM = 0xA7;
    pub const BROWSER_REFRESH: WPARAM = 0xA8;
    pub const BROWSER_STOP: WPARAM = 0xA9;
    pub const BROWSER_SEARCH: WPARAM = 0xAA;
    pub const BROWSER_FAVORITES: WPARAM = 0xAB;
    pub const BROWSER_HOME: WPARAM = 0xAC;
    pub const VOLUME_MUTE: WPARAM = 0xAD;
    pub const VOLUME_DOWN: WPARAM = 0xAE;
    pub const VOLUME_UP: WPARAM = 0xAF;
    pub const MEDIA_NEXT_TRACK: WPARAM = 0xB0;
    pub const MEDIA_PREV_TRACK: WPARAM = 0xB1;
    pub const MEDIA_STOP: WPARAM = 0xB2;
    pub const MEDIA_PLAY_PAUSE: WPARAM = 0xB3;
    pub const LAUNCH_MAIL: WPARAM = 0xB4;
    pub const LAUNCH_MEDIA_SELECT: WPARAM = 0xB5;
    pub const LAUNCH_APP1: WPARAM = 0xB6;
    pub const LAUNCH_APP2: WPARAM = 0xB7;
    pub const OEM_1: WPARAM = 0xBA;
    pub const OEM_PLUS: WPARAM = 0xBB;
    pub const OEM_COMMA: WPARAM = 0xBC;
    pub const OEM_MINUS: WPARAM = 0xBD;
    pub const OEM_PERIOD: WPARAM = 0xBE;
    pub const OEM_2: WPARAM = 0xBF;
    pub const OEM_3: WPARAM = 0xC0;
    pub const OEM_4: WPARAM = 0xDB;
    pub const OEM_5: WPARAM = 0xDC;
    pub const OEM_6: WPARAM = 0xDD;
    pub const OEM_7: WPARAM = 0xDE;
    pub const OEM_8: WPARAM = 0xDF;
    pub const OEM_102: WPARAM = 0xE2;
    pub const PROCESSKEY: WPARAM = 0xE5;
    pub const PACKET: WPARAM = 0xE7;
    pub const ATTN: WPARAM = 0xF6;
    pub const CRSEL: WPARAM = 0xF7;
    pub const EXSEL: WPARAM = 0xF8;
    pub const EREOF: WPARAM = 0xF9;
    pub const PLAY: WPARAM = 0xFA;
    pub const ZOOM: WPARAM = 0xFB;
    pub const NONAME: WPARAM = 0xFC;
    pub const PA1: WPARAM = 0xFD;
    pub const OEM_CLEAR: WPARAM = 0xFE;
}

pub mod WM {
    use win32_types::UINT;

    pub const INPUT: UINT = 0x00FF;
    pub const CLOSE: UINT = 0x0010;
    pub const NCCREATE: UINT = 0x0081;
    pub const LBUTTONDOWN: UINT = 0x0201;
    pub const LBUTTONUP: UINT = 0x0202;
    pub const CHAR: UINT = 0x0102;
    pub const COMMAND: UINT = 0x0111;
    pub const DESTROY: UINT = 0x0002;
    pub const ERASEBKGND: UINT = 0x0014;
    pub const KEYDOWN: UINT = 0x0100;
    pub const KEYUP: UINT = 0x0101;
    pub const SYSKEYDOWN: UINT = 0x0104;
    pub const SYSKEYUP: UINT = 0x0105;
    pub const KILLFOCUS: UINT = 0x0008;
    pub const MBUTTONDOWN: UINT = 0x0207;
    pub const MBUTTONUP: UINT = 0x0208;
    pub const MOUSEMOVE: UINT = 0x0200;
    pub const MOUSEWHEEL: UINT = 0x020A;
    pub const MOVE: UINT = 0x0003;
    pub const PAINT: UINT = 0x000F;
    pub const RBUTTONDOWN: UINT = 0x0204;
    pub const RBUTTONUP: UINT = 0x0205;
    pub const SETFOCUS: UINT = 0x0007;
    pub const SIZE: UINT = 0x0005;
    pub const SIZING: UINT = 0x0214;
    pub const ACTIVATEAPP: UINT = 0x001C;
}

#[link(name = "user32")]
extern "stdcall" {
    pub fn DefWindowProcW(hWnd: HWND, Msg: UINT, wParam: WPARAM, lParam: LPARAM) -> LRESULT;
    pub fn GetMessageW(lpMsg: *const MSG, hWnd: HWND, wMsgFilterMin: UINT, wMsgFilterMAx: UINT) -> BOOL;
    pub fn PeekMessageW(lpMsg: *const MSG, hWnd: HWND,wMsgFilterMin: UINT, wMsgFilterMAx: UINT, wRemoveMsg: UINT) -> BOOL;
    pub fn PostMessageW(hWnd: HWND, Msg: UINT, wParam: WPARAM, lParam: LPARAM) -> BOOL;
    pub fn TranslateMessage(lpMsg: *const MSG) -> BOOL;
    pub fn DispatchMessageW(lpMsg: *const MSG) -> LRESULT;
    pub fn SendMessageW(window: HWND, msg: UINT, wparam: WPARAM, lparam: LPARAM) -> LRESULT;
    pub fn PostQuitMessage(exitCode: c_int);
}
